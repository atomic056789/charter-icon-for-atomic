# Clone Hero Icons and Sources

[Link to preview all sources and icons](https://clonehero.gitlab.io/sources)

## Icon File Size and Dimensions

Overall we recommend using 64x64 PNG file for your icons. However if your icon is smaller than 12Kb then you may use a larger icon up to 128x128. There are some cases where a larger icon will look crisper in game. However since players will need to download any updated icons post release we do want to have sure icons are not too large as there is a lot of them.

Your icon will be automatically ran through the PNG optimization tool pngquant before being published so if you were looking to run your icon through that before hand the submission guidelines for size already take this into account to give a bit extra room.

## Testing Your Icon In Game

Inside your game folder you should find the following folders: `Custom\Game Icons` place your icon here and use a chart set to the name of icon file name to test the icon in game. Once happy with the icon you can then submit the icon here. 

## Adding an icon and/or a source

1. "Sign in / Register" to GitLab  
   ![](readme/tuto-1.png)
2. Easiest way is to log in with your Google Account (as you most likely have one), otherwise you can create an account or use an existing one.
3. Fork the repository: this is going to create a copy of the original repository so you can make changes on it, and submit it for review  
   ![](readme/tuto-2.png)
4. If you want to add an icon, navigate to the `/public/icons` folder of the repo
5. To add a file: "+" > "Upload file"  
   ![](readme/tuto-3.png)  
   To replace or delete a file: you will have to go the file directly instead, and click "Replace"/"Delete" there  
   ![](readme/tuto-4.png)
6. For the commit message, you can describe your changes, e.g. the setlist it's for and so on.
7. If you want to add a source, go to `/public/sources.txt` and edit the file. You can follow the formatting of the other entries (i.e. `icon_value '=' Source Name in Game`).  
   ![](readme/tuto-5.png)
8. If you are done with your changes, you can submit a merge request!  
   ![](readme/tuto-6.png)
   ![](readme/tuto-7.png)
9. Write a sensible description and send it our way: we'll approve the request when we can!  
   ![](readme/tuto-8.png)
